from django.shortcuts import render, redirect, get_object_or_404
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


@login_required
def createTask(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()  # tasks will be assigned to users
    context = {"form": form}
    return render(request, "tasks/createtask.html", context)


@login_required
def updateTask(request, pk):
    task = get_object_or_404(Task, id=pk)
    if request.method== 'POST':
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("show_my_task", pk=task.id)
    else:
        form = TaskForm(instance=task)
    context = {"task": task, "form": form}
    return render(request, "tasks/updatetask.html", context)


@login_required
def viewTasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/taskslist.html", context)


@login_required
def viewTask(request, pk):
    task = get_object_or_404(Task, id=pk)
    context = {"task": task}
    return render(request, "tasks/taskdetail.html", context)
