from django.urls import path
from tasks.views import createTask, viewTasks, viewTask, updateTask


urlpatterns = [
    path("create/", createTask, name="create_task"),
    path("mine/", viewTasks, name="show_my_tasks"),
    path("mine/<int:pk>/", viewTask, name="show_my_task"),
    path("mine/<int:pk>/update/", updateTask, name="update_task")
]
