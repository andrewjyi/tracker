from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project, Company
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm, ProjectByCompanyForm, SearchProjectsByCompanyForm


@login_required
def viewProjects(request):
    projects = Project.objects.filter(owner=request.user)
    if request.method != "POST":
        form = SearchProjectsByCompanyForm()
    else:
        form = SearchProjectsByCompanyForm(request.POST)
        if form.is_valid():
            name= form.cleaned_data["name"]
            company = Company.objects.filter(name=name)[0]
            id = company.id
            companyProjects = Project.objects.filter(company=id).filter(owner=request.user)
            context = {"companyProjects": companyProjects, "projects": projects, "form": form}
            return render(request, "projects/projectslist.html", context)
    context = {"projects": projects, "form": form}
    return render(request, "projects/projectslist.html", context)


@login_required
def viewProject(request, pk):
    project = get_object_or_404(Project, id=pk)
    context = {"project": project}
    return render(request, "projects/projectdetail.html", context)


@login_required
def viewProjectTimeline(request, pk):
    project = get_object_or_404(Project, id=pk)
    completedTasks= project.tasks.filter(is_completed=True)
    context = {"project": project, "completedtasks": completedTasks}
    return render(request, "projects/projecttimeline.html", context)


@login_required
def createProject(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()  # may not need to designate project owner
            return redirect("list_projects")
    else:
        form = ProjectForm(
            initial={"owner": request.user}
        )  # still shows other users
    context = {"form": form}
    return render(request, "projects/createproject.html", context)

#asdf
