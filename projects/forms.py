from django.forms import ModelForm
from projects.models import Project
from django import forms


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner", "company"]


class ProjectByCompanyForm(ModelForm):
    class Meta:
        model = Project
        exclude = ["name", "description", "owner"]

class SearchProjectsByCompanyForm(forms.Form):
    name = forms.CharField(max_length=150)
    
