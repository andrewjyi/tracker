from django.urls import path
from projects.views import viewProjects, viewProject, createProject, viewProjectTimeline


urlpatterns = [
    path("", viewProjects, name="list_projects"),
    path("<int:pk>/", viewProject, name="show_project"),
    path("create/", createProject, name="create_project"),
    path("<int:pk>/timeline", viewProjectTimeline, name="project_timeline")
]
